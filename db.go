package main

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

func getDatabase() *sql.DB {
	database, err := sql.Open("sqlite3", "./podcasts.db")
	if err != nil {
		panic(err)
	}
	return database
}

func prepareStatement(database *sql.DB, query string) *sql.Stmt {
	statement, err := database.Prepare(query)
	if err != nil {
		panic(err)
	}
	return statement
}
func initDB() {
	// create table if does not exist."~/.config/godcast/podcasts.db"
	database := getDatabase()
	statement := prepareStatement(database, "create table if not exists podcasts (id integer primary key, name text, url text)")
	statement.Exec()
	statement = prepareStatement(database, "create table if not exists episodes(id integer primary key, name text, title text, link text, description text, listened integer)")
	statement.Exec()
}

func insertPodcast(podcast *Podcast, url string) {
	database := getDatabase()
	statement := prepareStatement(database, "insert into podcasts(name, url) values (?,?)")
	title := podcast.Title
	statement.Exec(title, url)
}

func insertEpisodes(podcast *Podcast) {
	database := getDatabase()
	statement := prepareStatement(database, "insert into episodes(name, title, link, description) values (?,?,?,?)")
	for _, episode := range podcast.Episodes {
		statement.Exec(podcast.Title, episode.Title, episode.Link, episode.Description)
	}
}

// get podcasts and return
func getPodcasts() []Podcast {
	database := getDatabase()
	podcasts := []Podcast{}
	rows, err := database.Query("select name from podcasts")
	if err != nil {
		panic(err)
	}
	podcast := Podcast{}
	for rows.Next() {
		rows.Scan(&podcast.Title)
		podcasts = append(podcasts, podcast)
	}
	return podcasts
}

func getEpisodes(podcastName string) []Episode {
	database := getDatabase()
	episodes := []Episode{}
	rows, err := database.Query("select title, link, description from episodes where name like ?", podcastName)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		episode := Episode{}
		rows.Scan(&episode.Title, &episode.Link, &episode.Description)
		episodes = append(episodes, episode)
	}
	return episodes
}

func getEpisode(episodeName string) []string {
	database := getDatabase()
	rows, err := database.Query("select link from episodes where title like ?", episodeName)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	links := []string{}
	for rows.Next() {
		var link string
		rows.Scan(&link)
		links = append(links, link)
	}
	return links
}
