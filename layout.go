package main

import (
	"fmt"
	"log"

	"github.com/blang/mpv"
	"github.com/jroimartin/gocui"
)

func keybindings(g *gocui.Gui) {
	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("", gocui.KeyCtrlN, gocui.ModNone, popup); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("popup", gocui.KeyCtrlN, gocui.ModNone, closePopup); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("popup", gocui.KeyEnter, gocui.ModNone, savePodcast); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("podcasts", 'l', gocui.ModNone, nextView); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("podcasts", 'j', gocui.ModNone, cursorDownPod); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("podcasts", 'k', gocui.ModNone, cursorUpPod); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("episodes", 'h', gocui.ModNone, nextView); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("episodes", 'j', gocui.ModNone, cursorDown); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("episodes", 'k', gocui.ModNone, cursorUp); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("episodes", gocui.KeyEnter, gocui.ModNone, playEpisode); err != nil {
		log.Panicln(err)
	}
	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func closePopup(g *gocui.Gui, v *gocui.View) error {
	if err := g.DeleteView("popup"); err != nil {
		return err
	}
	if _, err := g.SetCurrentView("podcasts"); err != nil {
		return err
	}
	return nil
}

func popup(g *gocui.Gui, v *gocui.View) error {
	maxX, maxY := g.Size()
	if v, err := g.SetView("popup", maxX/2-40, maxY/2, maxX/2+20, maxY/2+2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "rss"
		v.Highlight = true
		v.Editable = true
		fmt.Fprintln(v, "")

		if _, err := g.SetCurrentView("popup"); err != nil {
			return err
		}

	}
	return nil
}

func savePodcast(g *gocui.Gui, v *gocui.View) error {
	var l string
	var err error
	_, cy := v.Cursor()
	if l, err = v.Line(cy); err != nil {
		l = ""
	}
	_, err = CreatePodcast(l)
	if err != nil {
		v.Highlight = true
		v.FgColor = gocui.ColorRed
		v.Title = "ERROR!!! COULD NOT RETRIEVE FEED"
		if l, err = v.Line(cy); err != nil {
			l = ""
		}
	} else {
		updatePodcasts(g, getPodcasts())
		if err := g.DeleteView("popup"); err != nil {
			return err
		}
		if _, err := g.SetCurrentView("podcasts"); err != nil {
			return err
		}
	}
	return nil
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	// podcast tab
	if v, err := g.SetView("podcasts", 0, 0, maxX/3-10, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "podcasts"
		v.Wrap = true
		v.Highlight = true
		v.SelBgColor = gocui.ColorGreen
		v.SelFgColor = gocui.ColorBlack
	}
	// episodes tab
	if v, err := g.SetView("episodes", maxX/3-9, 0, maxX-1, maxY-10); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "episodes"
		v.Wrap = true
		v.Highlight = true
		v.SelBgColor = gocui.ColorGreen
		v.SelFgColor = gocui.ColorBlack
		if _, err = setCurrentViewOnTop(g, "episodes"); err != nil {
			return err
		}
	}
	return nil
}

func setCurrentViewOnTop(g *gocui.Gui, name string) (*gocui.View, error) {
	if _, err := g.SetCurrentView(name); err != nil {
		return nil, err
	}
	return g.SetViewOnTop(name)
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func nextView(g *gocui.Gui, v *gocui.View) error {
	if v == nil || v.Name() == "podcasts" {
		_, err := g.SetCurrentView("episodes")
		return err
	}
	_, err := g.SetCurrentView("podcasts")
	return err
}

func updatePodcasts(g *gocui.Gui, podcasts []Podcast) {
	g.Update(func(g *gocui.Gui) error {
		v, err := g.View("podcasts")
		if err != nil {
			return err
		}
		v.Clear()
		for _, podcast := range getPodcasts() {
			fmt.Fprint(v, fmt.Sprintf("%s\n", podcast.Title))
		}
		return nil
	})
}

func updateList(g *gocui.Gui, p *Podcast) {
	g.Update(func(g *gocui.Gui) error {
		v, err := g.View("episodes")
		if err != nil {
			return err
		}
		v.Clear()
		for _, episode := range getEpisodes(p.Title) {
			fmt.Fprint(v, fmt.Sprintf("%s\n", episode.Title))
		}
		return nil
	})
}

func cursorDown(g *gocui.Gui, v *gocui.View) error {
	if v != nil {
		if lineBelow(g, v) {
			cx, cy := v.Cursor()
			n, _ := v.Line(cy + 1)
			if v.Title == "podcasts" {
				podcast := Podcast{n, getEpisodes(n)}
				updateList(g, &podcast)
			}
			if err := v.SetCursor(cx, cy+1); err != nil {
				ox, oy := v.Origin()
				if err := v.SetOrigin(ox, oy+1); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func cursorUp(g *gocui.Gui, v *gocui.View) error {
	if v != nil {
		ox, oy := v.Origin()
		cx, cy := v.Cursor()
		n, _ := v.Line(cy - 1)
		if v.Title == "podcasts" {
			podcast := Podcast{n, getEpisodes(n)}
			updateList(g, &podcast)
		}
		if err := v.SetCursor(cx, cy-1); err != nil && oy > 0 {
			if err := v.SetOrigin(ox, oy-1); err != nil {
				return err
			}
			// get the line and get info from episode
			n, _ := v.Line(oy)
			getEpisode(n)
		}
	}
	return nil
}

func cursorDownPod(g *gocui.Gui, v *gocui.View) error {
	return cursorDown(g, v)
}

func cursorUpPod(g *gocui.Gui, v *gocui.View) error {
	return cursorUp(g, v)
}

func lineBelow(g *gocui.Gui, v *gocui.View) bool {
	_, cy := v.Cursor()
	if l, _ := v.Line(cy + 1); l != "" {
		return true
	}
	return false
}

func playEpisode(g *gocui.Gui, v *gocui.View) error {
	_, cy := v.Cursor()
	n, _ := v.Line(cy)
	links := getEpisode(n)
	if len(links) > 0 {
		player.Loadfile(links[0], mpv.LoadFileModeReplace)
		player.SetPause(false)
	}
	return nil
}
