package main

import (
	gofeed "github.com/mmcdole/gofeed"
)

// Episode encloses the data of an episode
type Episode struct {
	Title       string
	Link        string
	Description string
}

// Podcast contain title and list of Episodes
type Podcast struct {
	Title    string
	Episodes []Episode
}

// CreatePodcast given a url create a podcast struct
func CreatePodcast(url string) (Podcast, error) {
	podcast := &Podcast{}
	gf := gofeed.NewParser()
	feed, err := gf.ParseURL(url)
	if err != nil {
		return *podcast, err
	}
	podcast.Title = feed.Title
	insertPodcast(podcast, url)
	for _, item := range feed.Items {
		// fmt.Printf("%d\n", index)
		for _, value := range item.Enclosures {
			if value.Type == "audio/mpeg" {
				episode := Episode{item.Title, value.URL, item.Description}
				podcast.Episodes = append(podcast.Episodes, episode)
			}
		}
	}
	insertEpisodes(podcast)
	return *podcast, nil
}
