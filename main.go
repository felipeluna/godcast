package main

import (
	"log"
	"os/exec"

	"github.com/blang/mpv"

	"github.com/jroimartin/gocui"
)

var player = startMpv()

func startMpv() *mpv.Client {
	cmd := exec.Command("mpv", "--idle", "--input-ipc-server=/tmp/mpvsocket")
	cmd.Start()
	ipcc := mpv.NewIPCClient("/tmp/mpvsocket") // Lowlevel client
	c := mpv.NewClient(ipcc)                   // Highlevel client, can also use RPCClient
	return c
}

func main() {
	// create new gui
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()
	g.Cursor = true
	// create db if doesn't exist
	initDB()
	// get podcasts and populate the gui
	podcasts := getPodcasts()
	updatePodcasts(g, podcasts)
	updateList(g, &podcasts[0])

	// set layout
	g.SetManagerFunc(layout)

	// run  keybindings
	keybindings(g)
}
